#Tkinter importieren
import tkinter as tk
from tkinter import ttk
from tkinter import *
import os
import smbus # für i2c
import datetime
import time
import pandas as pd
import numpy as np
import matplotlib
matplotlib.use('TkAgg')
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import  RPi.GPIO as GPIO # verwendung gpio pins
import shutil as sh
import serial
import socket
import csv
import gc
import threading
import re

class ControlFrame(tk.Frame):
    """
    Klasse, die das Kontroll-Frame darsetellen soll. Beinhaltet die Buttons zur Steuerung des Programms.
    Erbt von der Klasse tk.Frame
    """
    
    def __init__(self,masterFrame,owner,isIntendedToUseRelais):
        
        #Variable, speuchert ob Button fuer die Relais mit integriert werden soll
        self.__isIntendedToUseRelais = isIntendedToUseRelais
        
        #Aufruf Konstuktor der Eltern-Klasse:
        tk.Frame.__init__(self,masterFrame, relief = 'sunken', bd = 1) 
        
        #uebergeordnetes Fenster, bzw. Fenster, in welches dieser Frame platziert wird
        self.__masterFrame = masterFrame
        
        #Klasse, zu der Dieses Fenster gehoert. Owner enthaelt die Funktionen, welche in den Buttons aufgerufen werden
        self.__owner = owner
            
        #Gestalten des Control Frames

        #Festlegen der Textfelder(label)
        self.__lbData=tk.Label(self,text="Datenaufzeichnung", font ='bold')
        self.__lbGlobal=tk.Label(self,text="Gesamtprogramm", font='bold')
        self.__lbGlobal0=tk.Label(self,text="Panel auswählen", font='bold')
        #self.__lbPlot=tk.Label(self,text="Graphen", font='bold')
        
        #Falls Regelung mit verwendet werden soll
        if self.__isIntendedToUseRelais == True:
            #Erstelle die noetigen Textfelder
            self.__lbControl=tk.Label(self,text="Regelung", font='bold')
            
            #falls Relais mit angeschlossen werden sollen:  
            self.__lbRelayOne=tk.Label(self,text="Hauptschalter ", font='bold')
            #self.__lbRelayTwo=tk.Label(self,text="Hauptschalter", font='bold')
            self.__lbRelayThree=tk.Label(self,text="Heizmatte 1", font='bold')
            self.__lbRelayFour=tk.Label(self,text="Heizmatte 2", font='bold')
            self.__lbRelayFive=tk.Label(self,text="Stromzähler", font='bold')
            self.__lbRelaySix=tk.Label(self,text="Heizung IR-Sensor 1", font='bold')
            self.__lbRelaySeven=tk.Label(self,text="Heizung IR-Sensor 2", font='bold')
            #self.__lbRelayEight=tk.Label(self,text="Heizung IPU", font='bold')
            self.__lbRelayNine=tk.Label(self,text="IR-Sensor 1", font='bold')
            self.__lbRelayTen=tk.Label(self,text="IR-Sensor 2", font='bold')
            self.__lbRelayEleven=tk.Label(self,text="IR-Sensor 1 wählen", font='bold')
            self.__lbRelayTwelve=tk.Label(self,text="IR-Sensor 2 wählen", font='bold')


        #Definieren der Buttons, die in Frame sein sollen
        
        #Zum Beenden des gesammten Programms
        self.__bEnd = tk.Button(self, text =  'Programm Beenden ', command = self.__owner.endProgram)

        
        
        # falls Relais mit angeschlossen werden sollen:
        if self.__isIntendedToUseRelais == True:
                
            #self.__bTurnOnOffRelay = tk.Button(self, text='Einschalten', command = self.__owner.turnRelayOn)
    
            #Um Relais Anzuschalten
            self.__bTurnOnOffRelayOne = tk.Button(self, text='Einschalten', command = self.__owner.turnRelayOneOn)
            #Um Relais Auszuschalten
            #self.__bTurnOnOffRelayOne = tk.Button(self, text='Aus', command = self.__owner.turnRelayOff)

            #Um Relais Anzuschalten
            #self.__bTurnOnOffRelayTwo = tk.Button(self, text='Einschalten', command = self.__owner.turnRelayTwoOn)
            #Um Relais Auszuschalten
            #self.__bTurnOnOffRelayTwo = tk.Button(self, text='Aus', command = self.__owner.turnRelayOff)
            
            #Um Relais Anzuschalten
            self.__bTurnOnOffRelayThree = tk.Button(self, text='Einschalten', command = self.__owner.turnRelayThreeOn)
            #Um Relais Auszuschalten
            #bTurnOnOffRelayThree = tk.Button(frSetup, text='Aus', command = turnRelayOff)
            
            #Um Relais Anzuschalten
            self.__bTurnOnOffRelayFour = tk.Button(self, text='Einschalten', command = self.__owner.turnRelayFourOn)
            #Um Relais Auszuschalten
            #bTurnOnOffRelayFour = tk.Button(frSetup, text='Aus', command = turnRelayOff)
            
            #Um Relais Anzuschalten
            self.__bTurnOnOffRelayFive = tk.Button(self, text='Einschalten', command = self.__owner.turnRelayFiveOn)
            #Um Relais Auszuschalten
            #bTurnOnOffRelayFour = tk.Button(frSetup, text='Aus', command = turnRelayOff)
            
            #Um Relais Anzuschalten
            self.__bTurnOnOffRelaySix = tk.Button(self, text='Einschalten', command = self.__owner.turnRelaySixOn)
            #Um Relais Auszuschalten
            #bTurnOnOffRelayFour = tk.Button(frSetup, text='Aus', command = turnRelayOff)
            
            #Um Relais Anzuschalten
            self.__bTurnOnOffRelaySeven = tk.Button(self, text='Einschalten', command = self.__owner.turnRelaySevenOn)
            #Um Relais Auszuschalten
            #bTurnOnOffRelayFour = tk.Button(frSetup, text='Aus', command = turnRelayOff)
            
            #Um Relais Anzuschalten
            #self.__bTurnOnOffRelayEight = tk.Button(self, text='Einschalten', command = self.__owner.turnRelayEightOn)
            #Um Relais Auszuschalten
            #bTurnOnOffRelayFour = tk.Button(frSetup, text='Aus', command = turnRelayOff)

            #Um Relais Anzuschalten
            self.__bTurnOnOffRelayNine = tk.Button(self, text='IR-Sensor 1 An', command = self.__owner.turnRelayNineOn)
            #Um Relais Auszuschalten
            #bTurnOnOffRelayFour = tk.Button(frSetup, text='Aus', command = turnRelayOff)
            
            #Um Relais Anzuschalten
            self.__bTurnOnOffRelayTen = tk.Button(self, text='IR-Sensor 2 An', command = self.__owner.turnRelayTenOn)
            #Um Relais Auszuschalten
            #bTurnOnOffRelayFour = tk.Button(frSetup, text='Aus', command = turnRelayOff)
            
            
                        
            #Um Relais Anzuschalten
            self.__bHyDry = tk.Button(self, text='HyDry Relais An', command = self.__owner.turnOnThreeRelays)

            self.__bEdge = tk.Button(self, text='Edge Relais An', command = self.__owner.turnOnFiveRelays)
            
            
            var=tk.IntVar()
            #Um Relais 11 zu wechsen
            self.__bswitchintern1 = tk.Radiobutton(self, text='Sensor 1 Intern', variable=var, value=1, command = self.__owner.switchNC1).grid(row=11, column=0, padx=10, pady=10) 
 
            self.__bswitchextern1 = tk.Radiobutton(self, text='Sensor 1 Extern', variable=var, value=2, command = self.__owner.switchNO1).grid(row=12, column=0, padx=10, pady=10)
                
            var1=tk.IntVar()
            #Um Relais 12 zu wechsen
            self.__bswitchintern2 = tk.Radiobutton(self, text='Sensor 2 Intern', variable=var1, value=1, command = self.__owner.switchNC2).grid(row=11, column=3, padx=10, pady=10) 

            self.__bswitchextern2 = tk.Radiobutton(self, text='Sensor 2 Extern', variable=var1, value=2, command = self.__owner.switchNO2).grid(row=12, column=3, padx=10, pady=10)
            
        #Positionen der Labels und Buttons im Frame festlegen

        # falls Relais mit angeschlossen werden sollen:
        if self.__isIntendedToUseRelais == True:
                
            #self.__lbRelay.grid(row=1,column=0,padx=10, pady=10)
            #self.__bTurnOnOffRelay.grid(row=2,column=0,pady=2)
            
            #Mittlerer Teil: Relais 1 - Hauptschalter L
            self.__lbRelayOne.grid(row=1,column=0,padx=10, pady=10)
            self.__bTurnOnOffRelayOne.grid(row=2,column=0,pady=2)

            #Mittlerer Teil: Relais 2 - Hauptschalter N
            #self.__lbRelayTwo.grid(row=3,column=0,padx=10, pady=10)
            #self.__bTurnOnOffRelayTwo.grid(row=4,column=0,pady=2)
            
            #Mittlerer Teil: Relais 3 - Heizmatte 1
            self.__lbRelayThree.grid(row=3,column=0,padx=10, pady=10)
            self.__bTurnOnOffRelayThree.grid(row=4,column=0,pady=2)
            
            #Mittlerer Teil: Relais 4 - Heizmatte 2
            self.__lbRelayFour.grid(row=5,column=0,padx=10, pady=10)
            self.__bTurnOnOffRelayFour.grid(row=6,column=0,pady=2)

            #Mittlerer Teil: Relais 9 - IR-Sensor 1a
            self.__lbRelayNine.grid(row=7,column=0,padx=10, pady=10)
            self.__bTurnOnOffRelayNine.grid(row=8,column=0,pady=2)      
            
            #Mittlerer Teil: Relais 11 - IR-Sensor 1b
            self.__lbRelayEleven.grid(row=9,column=0,padx=10, pady=10)
              

            #Mittlerer Teil: Relais 5 - Stromzähler
            self.__lbRelayFive.grid(row=1,column=3,padx=10, pady=10)
            self.__bTurnOnOffRelayFive.grid(row=2,column=3,pady=2)
            
            #Mittlerer Teil: Relais 6 - Heizung IR-Sensor 1
            self.__lbRelaySix.grid(row=3,column=3,padx=10, pady=10)
            self.__bTurnOnOffRelaySix.grid(row=4,column=3,pady=2)
            
            #Mittlerer Teil: Relais 7 - loT
            self.__lbRelaySeven.grid(row=5,column=3,padx=10, pady=10)
            self.__bTurnOnOffRelaySeven.grid(row=6,column=3,pady=2)
            
            #Mittlerer Teil: Relais 8 - BLE
            #self.__lbRelayEight.grid(row=7,column=3,padx=10, pady=10)
            #self.__bTurnOnOffRelayEight.grid(row=8,column=3,pady=2)
            
            #Mittlerer Teil: Relais 10 - IR-Sensor 2a
            self.__lbRelayTen.grid(row=7,column=3,padx=10, pady=10)
            self.__bTurnOnOffRelayTen.grid(row=8,column=3,pady=2)
                        
            #Mittlerer Teil: Relais 12 - IR-Sensor 2b
            self.__lbRelayTwelve.grid(row=9,column=3,padx=10, pady=10)
                 
        self.__lbGlobal0.grid(row=1,column=1,padx=10, pady=10)
        #Um Relais Anzuschalten
        self.__bHyDry.grid(row=2,column=1,padx=10,pady=10)
        #Um Relais Anzuschalten
        self.__bEdge.grid(row=3,column=1,padx=10,pady=10)
              

            
        
        #Unterer Teil: Beenden des Gesammten Programms
        self.__lbGlobal.grid(row=13,column=1,padx=10, pady=10)
        self.__bEnd.grid(row=14,column=1,padx=10, pady=10)
        
        
    def pressedRelayOneOn(self):
        """
        Andert den Ralis Button, sodass bei naechstem Klick Relais ausgeschalten wird
        """
        
        self.__bTurnOnOffRelayOne.configure(text='Ausschalten', command = self.__owner.turnRelayOneOff)
    def pressedRelayTwoOn(self):
        self.__bTurnOnOffRelayTwo.configure(text='Ausschalten', command = self.__owner.turnRelayTwoOff)
    def pressedRelayThreeOn(self):
        self.__bTurnOnOffRelayThree.configure(text='Ausschalten', command = self.__owner.turnRelayThreeOff)
    def pressedRelayFourOn(self):
        self.__bTurnOnOffRelayFour.configure(text='Ausschalten', command = self.__owner.turnRelayFourOff)
    def pressedRelayFiveOn(self):
        self.__bTurnOnOffRelayFive.configure(text='Ausschalten', command = self.__owner.turnRelayFiveOff)
    def pressedRelaySixOn(self):
        self.__bTurnOnOffRelaySix.configure(text='Ausschalten', command = self.__owner.turnRelaySixOff)
    def pressedRelaySevenOn(self):
        self.__bTurnOnOffRelaySeven.configure(text='Ausschalten', command = self.__owner.turnRelaySevenOff)
    def pressedRelayEightOn(self):
        self.__bTurnOnOffRelayEight.configure(text='Ausschalten', command = self.__owner.turnRelayEightOff)
    def pressedRelayNineOn(self):
        self.__bTurnOnOffRelayNine.configure(text='IR-Sensor 1 Aus', command = self.__owner.turnRelayNineOff)
    def pressedRelayTenOn(self):
        self.__bTurnOnOffRelayTen.configure(text='IR-Sensor 2 Aus' , command = self.__owner.turnRelayTenOff)
    def pressedhyDryOn(self):
        self.__bHyDry.configure(text='HyDry Relais Aus' , command = self.__owner.turnOffThreeRelays)
        self.__bTurnOnOffRelayThree.configure(text='Ausschalten', command = self.__owner.turnRelayThreeOff)
        self.__bTurnOnOffRelayFive.configure(text='Ausschalten', command = self.__owner.turnRelayFiveOff)
        self.__bTurnOnOffRelayNine.configure(text='IR-Sensor 1 Aus', command = self.__owner.turnRelayNineOff)
    def pressededgeOn(self):
        self.__bEdge.configure(text='Edge Relais Aus' , command = self.__owner.turnOffFiveRelays)
        self.__bTurnOnOffRelayThree.configure(text='Ausschalten', command = self.__owner.turnRelayThreeOff)
        self.__bTurnOnOffRelayFour.configure(text='Ausschalten', command = self.__owner.turnRelayFourOff)
        self.__bTurnOnOffRelayFive.configure(text='Ausschalten', command = self.__owner.turnRelayFiveOff)
        self.__bTurnOnOffRelayNine.configure(text='IR-Sensor 1 Aus', command = self.__owner.turnRelayNineOff)
        self.__bTurnOnOffRelayTen.configure(text='IR-Sensor 2 Aus' , command = self.__owner.turnRelayTenOff)
        


    def pressedRelayOneOff(self):
        """
        Andert den Ralis Button, sodass bei naechstem Klick Relais angeschaltet wird
        """
        
        self.__bTurnOnOffRelayOne.configure(text='Einschalten', command = self.__owner.turnRelayOneOn)
    def pressedRelayTwoOff(self):
        self.__bTurnOnOffRelayTwo.configure(text='Einschalten', command = self.__owner.turnRelayTwoOn)
    def pressedRelayThreeOff(self):
        self.__bTurnOnOffRelayThree.configure(text='Einschalten', command = self.__owner.turnRelayThreeOn)
    def pressedRelayFourOff(self):        
        self.__bTurnOnOffRelayFour.configure(text='Einschalten', command = self.__owner.turnRelayFourOn)
    def pressedRelayFiveOff(self):        
        self.__bTurnOnOffRelayFive.configure(text='Einschalten', command = self.__owner.turnRelayFiveOn)
    def pressedRelaySixOff(self):        
        self.__bTurnOnOffRelaySix.configure(text='Einschalten', command = self.__owner.turnRelaySixOn)
    def pressedRelaySevenOff(self):        
        self.__bTurnOnOffRelaySeven.configure(text='Einschalten', command = self.__owner.turnRelaySevenOn)
    def pressedRelayEightOff(self):        
        self.__bTurnOnOffRelayEight.configure(text='Einschalten', command = self.__owner.turnRelayEightOn)
    def pressedRelayNineOff(self):        
        self.__bTurnOnOffRelayNine.configure(text='IR-Sensor 1 An', command = self.__owner.turnRelayNineOn)
    def pressedRelayTenOff(self):        
        self.__bTurnOnOffRelayTen.configure(text='IR-Sensor 2 An', command = self.__owner.turnRelayTenOn)
    def pressedhyDryOff(self):
        self.__bHyDry.configure(text='HyDry Relais Ein' , command = self.__owner.turnOnThreeRelays)
        self.__bTurnOnOffRelayThree.configure(text='Einschalten', command = self.__owner.turnRelayThreeOn)
        self.__bTurnOnOffRelayFive.configure(text='Einschalten', command = self.__owner.turnRelayFiveOn)
        self.__bTurnOnOffRelayNine.configure(text='IR-Sensor 1 An', command = self.__owner.turnRelayNineOn)

    def pressededgeOff(self):
        self.__bEdge.configure(text='Edge Relais Ein' , command = self.__owner.turnOnFiveRelays)
        self.__bTurnOnOffRelayThree.configure(text='Einschalten', command = self.__owner.turnRelayThreeOn)
        self.__bTurnOnOffRelayFour.configure(text='Einschalten', command = self.__owner.turnRelayFourOn)
        self.__bTurnOnOffRelayFive.configure(text='Einschalten', command = self.__owner.turnRelayFiveOn)
        self.__bTurnOnOffRelayNine.configure(text='IR-Sensor 1 An', command = self.__owner.turnRelayNineOn)
        self.__bTurnOnOffRelayTen.configure(text='IR-Sensor 2 An', command = self.__owner.turnRelayTenOn)
        
    def pressedendProgram(self):        
        self.__bEnd.configure(command = self.__owner.endProgram)
        
        

class MainLogFrame(tk.Frame):
    """
    Frame, das die Darstellung des Main-Log uebernimmt. Erbt von tk.Frame. In das Main-Log werden alle wichtigen Erignisse geschrieben
    
    TODO: vermutlich noch ganz gut den inhlat des Main-Logs in eigener Datei abzulegen
    """
        
    #Klassenvariablen Hoehe und Breite des Logs
    __mainDataLabelHeight = 20
    __mainDataLabelWidth = 20
    
    def __init__(self,masterFrame):
        """
        Initialisiert frame fuer Main-Log. Erstellt daas Textfeld und bindet Scrollbar an
        """
        
        #Aufruf Konstuktor der Eltern-Klasse:
        tk.Frame.__init__(self,masterFrame, relief = 'sunken', bd = 1) 
        
        # Gestaltung des frMainLog-Frames

        # log für bisherige Daten erstellen
        self.__mainDataLog = tk.Text(self, width=type(self).__mainDataLabelWidth, height=type(self).__mainDataLabelHeight, takefocus=0)

        self.__mainDataLog.pack(side = 'left',fill='both', expand=1)

        #Scrollbar erstellen
        self.__scrollbar = tk.Scrollbar(self)

        # attach text box to scrollbar
        self.__mainDataLog.config(yscrollcommand=self.__scrollbar.set)
        self.__scrollbar.config(command=self.__mainDataLog.yview)
        #Scrollbar an rechter Seite platzieren
        self.__scrollbar.pack(side = 'right', fill='y')
        
        
    def writeToLog(self, message):
        """
        Setzt uebergebene Message in Main-Log ein. Versieht die Message mit einem Zeitstempel.
        """
        
        t = datetime.datetime.now()
        t = t.strftime( '%d.%m.%Y %H:%M:%S ')
        self.__mainDataLog.insert('0.0', t +': ' + message + '\n')
        
        # TODO: Koennte Meldugnen auch gleich in ein Log-File eintragen. Koennte Protokoll erleichtern.
 

class InformWindow:
    def __init__(self,informStr):
        self.window = tk.Tk()
        self.window.title("Information")
        self.window.geometry("220x60")
        label = tk.Label(self.window, text=informStr)
        buttonOK = tk.Button(self.window,text="OK",command=self.processButtonOK)
        label.pack(side = tk.TOP)
        buttonOK.pack(side = tk.BOTTOM)
        self.window.mainloop()

    def processButtonOK(self):
        self.window.destroy()
        
class PanelUSBFrame(tk.Frame):
    def __init__(self,masterFrame):
        self.uartState = False # is uart open or not
        tk.Frame.__init__(self,masterFrame, relief = 'sunken', bd = 1) 
        labelCOM = tk.Label(self,text="COMx: ")
        self.COM = tk.StringVar(value = "/dev/ttyUSB0")
        ertryCOM = tk.Entry(self, textvariable = self.COM)
        labelCOM.grid(row = 1, column = 1, padx = 5, pady = 3)
        ertryCOM.grid(row = 2, column = 1, padx = 5, pady = 3)

        labelBaudrate = tk.Label(self,text="Baudrate: ")
        self.Baudrate = tk.IntVar(value = 460000)
        ertryBaudrate = tk.Entry(self, textvariable = self.Baudrate)
        labelBaudrate.grid(row = 3, column = 1, padx = 5, pady = 3)
        ertryBaudrate.grid(row = 4, column = 1, padx = 5, pady = 3)

        labelParity = tk.Label(self,text="Parity: ")
        self.Parity = tk.StringVar(value ="NONE")
        comboParity = ttk.Combobox(self, width = 17, textvariable=self.Parity)
        comboParity["values"] = ("NONE","ODD","EVEN","MARK","SPACE")
        comboParity["state"] = "readonly"
        #labelParity.grid(row = 5, column = 1, padx = 5, pady = 3)
        #comboParity.grid(row = 6, column = 1, padx = 5, pady = 3)

        labelStopbits = tk.Label(self,text="Stopbits: ")
        self.Stopbits = tk.StringVar(value ="1")
        comboStopbits = ttk.Combobox(self, width = 17, textvariable=self.Stopbits)
        comboStopbits["values"] = ("1","1.5","2")
        comboStopbits["state"] = "readonly"
        labelStopbits.grid(row = 5, column = 1, padx = 5, pady = 3)
        comboStopbits.grid(row = 6, column = 1, padx = 5, pady = 3)
        
        self.buttonSS = tk.Button(self, text = "Start", command = self.processButtonSS)
        self.buttonSS.grid(row = 7, column = 1,padx = 5, pady = 3, sticky = tk.E)

        # serial object
        self.ser = serial.Serial()
        # serial read threading
        self.ReadUARTThread = threading.Thread(target=self.ReadUART)
        self.ReadUARTThread.start()

        self.__labelOutText = tk.Label(self,text="Received Data:")
        self.__labelOutText.grid(row = 8, column = 1, padx = 5, pady = 3, sticky = tk.W)
        self.__scrollbarRecv = tk.Scrollbar(self)
        self.__OutputText = tk.Text(self, wrap = tk.WORD, width = 60, height = 40,yscrollcommand =self.__scrollbarRecv.set)
        self.__OutputText.grid(row = 9, column = 1, padx = 5, pady = 3)
        
        #Scrollbar erstellen
        self.__scrollbarRecv = tk.Scrollbar(self)
        # attach frame box to scrollbar
        self.__OutputText.config(yscrollcommand=self.__scrollbarRecv.set)
        self.__scrollbarRecv.config(command=self.__OutputText.yview)
        self.__scrollbarRecv.grid(row = 9, column = 3, stick=S + W + N)
        self.__OutputText.yview_moveto(20.0)


    def processButtonSS(self):
        #print(self.Parity.get())
        if (self.uartState):
            self.ser.close()
            self.buttonSS["text"] = "Start"
            self.uartState = False
        else:
            # restart serial port
            self.ser.port = self.COM.get()
            self.ser.baudrate = self.Baudrate.get()
            
            strParity = self.Parity.get()
            if (strParity=="NONE"):
                self.ser.parity = serial.PARITY_NONE;
            elif(strParity=="ODD"):
                self.ser.parity = serial.PARITY_ODD;
            elif(strParity=="EVEN"):
                self.ser.parity = serial.PARITY_EVEN;
            elif(strParity=="MARK"):
                self.ser.parity = serial.PARITY_MARK;
            elif(strParity=="SPACE"):
                self.ser.parity = serial.PARITY_SPACE;
                
            strStopbits = self.Stopbits.get()
            if (strStopbits == "1"):
                self.ser.stopbits = serial.STOPBITS_ONE;
            elif (strStopbits == "1.5"):
                self.ser.stopbits = serial.STOPBITS_ONE_POINT_FIVE;
            elif (strStopbits == "2"):
                self.ser.stopbits = serial.STOPBITS_TWO;
            
            try:
                self.ser.open()
            except:
                infromStr = "Can't open "+self.ser.port
                InformWindow(infromStr)
            
            if (self.ser.isOpen()): # open success
                self.buttonSS["text"] = "Stop"
                self.uartState = True

    def ReadUART(self):
        # print("Threading...")
        ti = datetime.datetime.now()
        ti = ti.strftime( '%d_%m_%Y %H:%M')
        path='/home/pi/Public/IPU-Teststand/OutputText/'+ti+'.txt'
        text=''
        while True:
            if (self.uartState):
                        ch = self.ser.readline().decode(encoding='ascii')
                        text=text+ch
                        t=datetime.datetime.now().strftime('%d.%m.%Y %H:%M:%S')
                        self.__OutputText.insert('0.0',t+':'+text)
                        #print(text,end='')
                        keyStart='IPU'
                        keyEnd='BATTERY'
                        pat=re.compile(keyStart+"(.*?)"+keyEnd,re.S)
                        result=pat.findall(text)
                        result_min=set(result)
                        #for i in result:
                                #if i not in result_min:
                                        #result_min.append(pat)
                        print('\n'.join(result_min),end='')
                        

                
                        f = open(path, 'a')
                        #t = datetime.datetime.now()
                        #t = t.strftime( '%d.%m.%Y %H:%M:%S')
                        #f.write(t+":"+"\n")
                        #f.write(t+"\n"+"IPU"+"\n".join(result_min)+"\n")
                        f.write(t+"\n"+"IPU"+str(result)+"\n")
                        result=''
                        f.close()



 

                        
                #except:
                    #infromStr = "Something wrong in receiving."
                    #InformWindow(infromStr)
                    #self.ser.close() # close the serial when catch exception
                    #self.buttonSS["text"] = "Start"
                    #self.uartState = False

 



                    
 

class MainGUI():
    """
    Repraesentiert die graphische Beutzeroberflaeche. Steuert den Programmablauf durch aufruf der Funktionen.
    """
    
    
    def __init__(self,isIntendedToUseRelais,isIntededToUseScaleUSB):
        """
        Erzeugt neues MainGUI-Objekt.
        Initialisiert das Hauptfenster (root) und platziert 4 Frames darin, welche die einzelnen Bestandteile der Benutzeroberflaeche darstellen
        Erzeugt ebenfalls ein Data-Management-Objekt, welches die Beschaffung, aufzeichnung und verarbetung der aufgezeichnetten Werte regelt.
        """
        
        #Variable die speichert, ob die Waage mit verwendet werden soll
        self.__isIntendedToUseRelais = isIntendedToUseRelais
        
        #Variable die speichert, ob Waage ueber USB oder serileer Schnittstelle ausgelesen werden soll 
        self.__isIntededToUseScaleUSB = isIntededToUseScaleUSB

        # erstellen des Haupt-Fensters
        self.__root = tk.Tk()
        self.__root.title('Kontroller des IPU-Teststand')

        
        self.__Controll=Actuators(self.__isIntendedToUseRelais)

        
        # Drei Frames erstellen, welche in der GUI dargestellt werden sollen
        
        # Soll Haupt-Datenlog anzeigen
        self.__frMainLog = MainLogFrame(self.__root)
        
        # Soll alle elemetne, die zur Steuerung benötigt werden darstellen
        self.__frControl = ControlFrame(self.__root,self, self.__isIntendedToUseRelais)
        
        # Soll USB-Data anzeigen
        self.__frPanelUSB = PanelUSBFrame(self.__root)
        
        
        #Platzieren der Frames
        #frCurrentData.pack(side = 'left', expand = 1, fill = 'y')
        self.__frPanelUSB.pack(side = 'left', expand = 0, fill = 'y')
        self.__frControl.pack(side = 'top', fill = 'x')
        self.__frMainLog.pack(side = 'right', expand= 1,fill = 'both')

        

    
    
        # Variablen zur Steuerung ds Programmablaufs
        self.__running = False  # Global flag
        self.__count = 0        # Global flag
        
        #Uebergabe eines ersten (default)wertes an 
        #Zeitpunkt des ersten Scan-Starts
        self.__firstScanStartTime =0
        

        
        # Speicherzyklus bei, jedem ... Wert, definiert durch div
        self.__saveAfterSoManyRecordings = 5
        


    
    # Funktionen:

    def startMainloop(self):
        """
        Startet die Mainloop der GUI
        """
        
        self.__root.mainloop()
    

        
    def turnRelayOneOn(self): 
        """
        Fuktion, die die beiden Relais anschalten soll. Aendert die angezeigten buttons und leitet Befehl an relais weiter 
        """
        #gib Stoppmeldung mit aktuellem Datum aus
        self.__frMainLog.writeToLog('Hauptschalter eingeschaltet')
        
        # Button aendern und jetzt mit Befehl fuer Stoppen der Aufzeichnung belegen
        self.__frControl.pressedRelayOneOn()
        
        # Relais 1 einschalten
        self.__Controll.getRelais().turnOnRelayOne()
    def turnRelayOneOff(self):
        """
        Fuktion, die die beiden Relais ausschalten soll. Aendert die angezeigten buttons und leitet Befehl an relais weiter 
        """
        #gib Stoppmeldung mit aktuellem Datum aus
        self.__frMainLog.writeToLog('Hauptschalter ausgeschaltet')
        
        # Button aendern und jetzt mit Befehl fuer Stoppen der Aufzeichnung belegen
        self.__frControl.pressedRelayOneOff()
        
        # Relais 1 ausschalten
        self.__Controll.getRelais().turnOffRelayOne()
        

#    def turnRelayTwoOn(self): 
        """
        Fuktion, die die beiden Relais anschalten soll. Aendert die angezeigten buttons und leitet Befehl an relais weiter 
        """
        #gib Stoppmeldung mit aktuellem Datum aus
#        self.__frMainLog.writeToLog('Hauptschalter N eingeschaltet')
        
        # Button aendern und jetzt mit Befehl fuer Stoppen der Aufzeichnung belegen
#        self.__frControl.pressedRelayTwoOn()
        
        # Relais einschalten
#        self.__Controll.getRelais().turnOnRelayTwo()

#    def turnRelayTwoOff(self):
        """
        Fuktion, die die beiden Relais aussc   halten soll. Aendert die angezeigten buttons und leitet Befehl an relais weiter 
        """
        #gib Stoppmeldung mit aktuellem Datum aus
#        self.__frMainLog.writeToLog('Hauptshaclter N ausgeschaltet')
        
        # Button aendern und jetzt mit Befehl fuer Stoppen der Aufzeichnung belegen
#        self.__frControl.pressedRelayTwoOff()
        
        # Relais ausschalten
#        self.__Controll.getRelais().turnOffRelayTwo()
        

    def turnRelayThreeOn(self): 
        """
        Fuktion, die die beiden Relais anschalten soll. Aendert die angezeigten buttons und leitet Befehl an relais weiter 
        """
        #gib Stoppmeldung mit aktuellem Datum aus
        self.__frMainLog.writeToLog('Heizmatte 1 eingeschaltet')
        
        # Button aendern und jetzt mit Befehl fuer Stoppen der Aufzeichnung belegen
        self.__frControl.pressedRelayThreeOn()

        # Relais 3 einschalten
        self.__Controll.getRelais().turnOnRelayThree()        

        
    def turnRelayThreeOff(self):
        """
        Fuktion, die die beiden Relais ausschalten soll. Aendert die angezeigten buttons und leitet Befehl an relais weiter 
        """
        #gib Stoppmeldung mit aktuellem Datum aus
        self.__frMainLog.writeToLog('Heizmatte 1 ausgeschaltet')
        
        # Button aendern und jetzt mit Befehl fuer Stoppen der Aufzeichnung belegen
        self.__frControl.pressedRelayThreeOff()
        
        # Relais 3 ausschalten
        self.__Controll.getRelais().turnOffRelayThree()

    def turnRelayFourOn(self): 
        """
        Fuktion, die die beiden Relais anschalten soll. Aendert die angezeigten buttons und leitet Befehl an relais weiter 
        """
        #gib Stoppmeldung mit aktuellem Datum aus
        self.__frMainLog.writeToLog('Heizmatte 2 eingeschaltet')
        
        # Button aendern und jetzt mit Befehl fuer Stoppen der Aufzeichnung belegen
        self.__frControl.pressedRelayFourOn()
        
        # Relais 4 einschalten
        self.__Controll.getRelais().turnOnRelayFour() 
        
    def turnRelayFourOff(self):
        """
        Fuktion, die die beiden Relais ausschalten soll. Aendert die angezeigten buttons und leitet Befehl an relais weiter 
        """
        #gib Stoppmeldung mit aktuellem Datum aus
        self.__frMainLog.writeToLog('Heizmatte 2 ausgeschaltet')
        
        # Button aendern und jetzt mit Befehl fuer Stoppen der Aufzeichnung belegen
        self.__frControl.pressedRelayFourOff()
        
        # Relais 4 ausschalten
        self.__Controll.getRelais().turnOffRelayFour()
        
    def turnRelayFiveOn(self): 
        """
        Fuktion, die die beiden Relais anschalten soll. Aendert die angezeigten buttons und leitet Befehl an relais weiter 
        """
        #gib Stoppmeldung mit aktuellem Datum aus
        self.__frMainLog.writeToLog('Stromzähler eingeschaltet')
        
        # Button aendern und jetzt mit Befehl fuer Stoppen der Aufzeichnung belegen
        self.__frControl.pressedRelayFiveOn()
          
        # Relais 5 einschalten
        self.__Controll.getRelais().turnOnRelayFive() 

    def turnRelayFiveOff(self):
        """
        Fuktion, die die beiden Relais ausschalten soll. Aendert die angezeigten buttons und leitet Befehl an relais weiter 
        """
        #gib Stoppmeldung mit aktuellem Datum aus
        self.__frMainLog.writeToLog('Stromzähler ausgeschaltet')
        
        # Button aendern und jetzt mit Befehl fuer Stoppen der Aufzeichnung belegen
        self.__frControl.pressedRelayFiveOff()
        
        # Relais 5 ausschalten
        self.__Controll.getRelais().turnOffRelayFive()

    def turnRelaySixOn(self): 
        """
        Fuktion, die die beiden Relais anschalten soll. Aendert die angezeigten buttons und leitet Befehl an relais weiter 
        """
        #gib Stoppmeldung mit aktuellem Datum aus
        self.__frMainLog.writeToLog('Heizung IR-Sensor 1 eingeschaltet')
        
        # Button aendern und jetzt mit Befehl fuer Stoppen der Aufzeichnung belegen
        self.__frControl.pressedRelaySixOn()
          
        # Relais 6 einschalten
        self.__Controll.getRelais().turnOnRelaySix() 

        
    def turnRelaySixOff(self):
        """
        Fuktion, die die beiden Relais ausschalten soll. Aendert die angezeigten buttons und leitet Befehl an relais weiter 
        """
        #gib Stoppmeldung mit aktuellem Datum aus
        self.__frMainLog.writeToLog('Heizung IR-Sensor 1 ausgeschaltet')
        
        # Button aendern und jetzt mit Befehl fuer Stoppen der Aufzeichnung belegen
        self.__frControl.pressedRelaySixOff()
        
        # Relais 6 ausschalten
        self.__Controll.getRelais().turnOffRelaySix()


    def turnRelaySevenOn(self): 
        """
        Fuktion, die die beiden Relais anschalten soll. Aendert die angezeigten buttons und leitet Befehl an relais weiter 
        """
        #gib Stoppmeldung mit aktuellem Datum aus
        self.__frMainLog.writeToLog('Heizung Sensor 2 eingeschaltet')
        
        # Button aendern und jetzt mit Befehl fuer Stoppen der Aufzeichnung belegen
        self.__frControl.pressedRelaySevenOn()

        # Relais 7 einschalten
        self.__Controll.getRelais().turnOnRelaySeven() 

    def turnRelaySevenOff(self):
        """
        Fuktion, die die beiden Relais ausschalten soll. Aendert die angezeigten buttons und leitet Befehl an relais weiter 
        """
        #gib Stoppmeldung mit aktuellem Datum aus
        self.__frMainLog.writeToLog('Heizung Sensor 2 ausgeschaltet')
        
        # Button aendern und jetzt mit Befehl fuer Stoppen der Aufzeichnung belegen
        self.__frControl.pressedRelaySevenOff()

        # Relais 7 ausschalten
        self.__Controll.getRelais().turnOffRelaySeven()

#    def turnRelayEightOn(self): 
        """
        Fuktion, die die beiden Relais anschalten soll. Aendert die angezeigten buttons und leitet Befehl an relais weiter 
        """
        #gib Stoppmeldung mit aktuellem Datum aus
#        self.__frMainLog.writeToLog('Heizung IPU eingeschaltet')
        
        # Button aendern und jetzt mit Befehl fuer Stoppen der Aufzeichnung belegen
#        self.__frControl.pressedRelayEightOn()

        # Relais 8 einschalten
#        self.__Controll.getRelais().turnOnRelayEight() 
        
#    def turnRelayEightOff(self):
        """
        Fuktion, die die beiden Relais ausschalten soll. Aendert die angezeigten buttons und leitet Befehl an relais weiter 
        """
        #gib Stoppmeldung mit aktuellem Datum aus
#        self.__frMainLog.writeToLog('Heizung IPU ausgeschaltet')
        
        # Button aendern und jetzt mit Befehl fuer Stoppen der Aufzeichnung belegen
#        self.__frControl.pressedRelayEightOff()

        # Relais 8 ausschalten
#        self.__Controll.getRelais().turnOffRelayEight()
        
    def turnRelayNineOn(self): 
        """
        Fuktion, die die beiden Relais anschalten soll. Aendert die angezeigten buttons und leitet Befehl an relais weiter 
        """
        #gib Stoppmeldung mit aktuellem Datum aus
        self.__frMainLog.writeToLog('IR-Sensor 1 An')
        
        # Button aendern und jetzt mit Befehl fuer Stoppen der Aufzeichnung belegen
        self.__frControl.pressedRelayNineOn()

        # Relais 9 einschalten
        self.__Controll.getRelais().turnOnRelayNine() 
        
    def turnRelayNineOff(self):
        """
        Fuktion, die die beiden Relais ausschalten soll. Aendert die angezeigten buttons und leitet Befehl an relais weiter 
        """
        #gib Stoppmeldung mit aktuellem Datum aus
        self.__frMainLog.writeToLog('IR-Sensor 1 Aus')
        
        # Button aendern und jetzt mit Befehl fuer Stoppen der Aufzeichnung belegen
        self.__frControl.pressedRelayNineOff()

        # Relais 9 ausschalten
        self.__Controll.getRelais().turnOffRelayNine()
        
    def turnRelayTenOn(self): 
        """
        Fuktion, die die beiden Relais anschalten soll. Aendert die angezeigten buttons und leitet Befehl an relais weiter 
        """
        #gib Stoppmeldung mit aktuellem Datum aus
        self.__frMainLog.writeToLog('IR-Sensor 2 An')
        
        # Button aendern und jetzt mit Befehl fuer Stoppen der Aufzeichnung belegen
        self.__frControl.pressedRelayTenOn()

        # Relais 10 einschalten
        self.__Controll.getRelais().turnOnRelayTen() 
        
    def turnRelayTenOff(self):
        """
        Fuktion, die die beiden Relais ausschalten soll. Aendert die angezeigten buttons und leitet Befehl an relais weiter 
        """
        #gib Stoppmeldung mit aktuellem Datum aus
        self.__frMainLog.writeToLog('IR-Sensor 2 Aus')
        
        # Button aendern und jetzt mit Befehl fuer Stoppen der Aufzeichnung belegen
        self.__frControl.pressedRelayTenOff()

        # Relais 10 ausschalten
        self.__Controll.getRelais().turnOffRelayTen()
        
    def switchNO1(self): 
        """
        Fuktion, die die beiden Relais anschalten soll. Aendert die angezeigten buttons und leitet Befehl an relais weiter 
        """
        #gib Stoppmeldung mit aktuellem Datum aus
        self.__frMainLog.writeToLog('IR-Sensor 1 Extern')
        
        # Button aendern und jetzt mit Befehl fuer Stoppen der Aufzeichnung belegen


        # Relais 11 einschalten
        self.__Controll.getRelais().SwitchNOOne() 
        
    def switchNC1(self): 
        """
        Fuktion, die die beiden Relais anschalten soll. Aendert die angezeigten buttons und leitet Befehl an relais weiter 
        """
        #gib Stoppmeldung mit aktuellem Datum aus
        self.__frMainLog.writeToLog('IR-Sensor 1 Intern')
        
        # Button aendern und jetzt mit Befehl fuer Stoppen der Aufzeichnung belegen


        # Relais 11 einschalten
        self.__Controll.getRelais().SwitchNCOne() 
        
    def switchNC2(self):
        """
        Fuktion, die die beiden Relais ausschalten soll. Aendert die angezeigten buttons und leitet Befehl an relais weiter 
        """
        #gib Stoppmeldung mit aktuellem Datum aus
        self.__frMainLog.writeToLog('IR-Sensor 2 Intern')
        


        # Relais 10 ausschalten
        self.__Controll.getRelais().SwitchNCTwo()

    def switchNO2(self):
        """
        Fuktion, die die beiden Relais ausschalten soll. Aendert die angezeigten buttons und leitet Befehl an relais weiter 
        """
        #gib Stoppmeldung mit aktuellem Datum aus
        self.__frMainLog.writeToLog('IR-Sensor 2 Extern')
        


        # Relais 10 ausschalten
        self.__Controll.getRelais().SwitchNOTwo()
        
    def turnOffThreeRelays(self):
        #gib Stoppmeldung mit aktuellem Datum aus
        self.__frMainLog.writeToLog('Stromzähler, Heizmatte 1, IR-Sensor 2 sind ausgeschaltet')
        
        self.__frControl.pressedhyDryOff()

        # Relais  ausschalten
        self.__Controll.getRelais().HyDryRelaisOff()

    def turnOffFiveRelays(self):
        #gib Stoppmeldung mit aktuellem Datum aus
        self.__frMainLog.writeToLog('Stromzähler, Heizmatte 1&2, IR-Sensor 1&2 sind ausgeschaltet')
        
        self.__frControl.pressededgeOff()

        # Relais ausschalten
        self.__Controll.getRelais().EdgeRelaisOff()
        
        
    def turnOnThreeRelays(self):
        #gib Stoppmeldung mit aktuellem Datum aus
        self.__frMainLog.writeToLog('Stromzähler, Heizmatte 1, IR-Sensor 1 sind eingeschaltet,bitte wählen Sie IN/EXTERN Sensor 1')
        
        self.__frControl.pressedhyDryOn()

        # Relais  ausschalten
        self.__Controll.getRelais().HyDryRelais()

    def turnOnFiveRelays(self):
        #gib Stoppmeldung mit aktuellem Datum aus
        self.__frMainLog.writeToLog('Stromzähler, Heizmatte 1&2, IR-Sensor 1&2 sind eingeschaltet,bitte wählen Sie IN/EXTERN Sensor 1 und 2')
        
        self.__frControl.pressededgeOn()

        # Relais ausschalten
        self.__Controll.getRelais().EdgeRelais()
        
    def endProgram(self):
        
        # Button aendern und jetzt mit Befehl fuer Stoppen der Aufzeichnung belegen
        self.__frControl.pressedendProgram()
        
        # teminates mainloop and deletes all widgets  quit() would stop TCL interpreter
        self.__root.destroy() 
        #schalte beide Relais aus
        self.__Controll.getRelais().turnAllRelaysOff()

        
class Relais():
    """
    Klasse die das Relasi repraesentiert. Relais soll auf befehl in an und ausgeschaltet werden koennen.
    
    Das Relais muss an 5V und Gnd angeschlossen werden. 
    Der Steuerkreis wird an "COM" angeschlossen und die Stromversorgung an "NO"
    
    Zudem muss die Steuerung des relais an den angegebenenen Control Pins angeschlossen werden.
    
    """
    
    # Setzen der Zaehlweise der pins (moegl. BOARD oder BCM)
    GPIO.setmode(GPIO.BCM)
    
    def __init__(self, controlPinOne, controlPinTwo, controlPinThree, controlPinFour, controlPinFive, controlPinSix, controlPinSeven, controlPinEight, controlPinNine, controlPinTen,controlPinEleven, controlPinTwelve):
        """
        Erzeugt Relais-Objekt. Benoetigt angebe der Steuerpins des relais. Sdhaelt relais zu beginn aus
        """
        
        #Pins festlegen, welche die Steuerung der Relais Uebernehmen.
        self.__controlPinOne = controlPinOne
        self.__controlPinTwo = controlPinTwo
        self.__controlPinThree = controlPinThree
        self.__controlPinFour = controlPinFour
        self.__controlPinFive = controlPinFive
        self.__controlPinSeven = controlPinSeven
        self.__controlPinSix = controlPinSix
        self.__controlPinEight = controlPinEight
        self.__controlPinNine = controlPinNine
        self.__controlPinTen = controlPinTen
        self.__controlPinEleven = controlPinEleven
        self.__controlPinTwelve = controlPinTwelve
        
        # aktivieren der GPIO-Pins und zu anfang auf HIGH setzen
        GPIO.setup(self.__controlPinOne , GPIO.OUT, initial = GPIO.HIGH)
        GPIO.setup(self.__controlPinTwo , GPIO.OUT, initial = GPIO.HIGH)
        GPIO.setup(self.__controlPinThree , GPIO.OUT, initial = GPIO.HIGH)
        GPIO.setup(self.__controlPinFour , GPIO.OUT, initial = GPIO.HIGH)
        GPIO.setup(self.__controlPinFive , GPIO.OUT, initial = GPIO.HIGH)
        GPIO.setup(self.__controlPinSix , GPIO.OUT, initial = GPIO.HIGH)
        GPIO.setup(self.__controlPinSeven , GPIO.OUT, initial = GPIO.HIGH)
        GPIO.setup(self.__controlPinEight , GPIO.OUT, initial = GPIO.HIGH)
        GPIO.setup(self.__controlPinNine , GPIO.OUT, initial = GPIO.HIGH)
        GPIO.setup(self.__controlPinTen , GPIO.OUT, initial = GPIO.HIGH)
        GPIO.setup(self.__controlPinEleven , GPIO.OUT, initial = GPIO.HIGH)
        GPIO.setup(self.__controlPinTwelve , GPIO.OUT, initial = GPIO.HIGH)

 
        time.sleep(0.1)
        
        
    def turnAllRelaysOn(self):
        """
        Soll Relais mit angegebener Nummer anschalten
        """
        
        # Setze beide Steuer-pins auf LOW
        GPIO.output(self.__controlPinOne,GPIO.LOW)
        GPIO.output(self.__controlPinTwo,GPIO.LOW)
        GPIO.output(self.__controlPinThree,GPIO.LOW)
        GPIO.output(self.__controlPinFour,GPIO.LOW)
        GPIO.output(self.__controlPinFive,GPIO.LOW)
        GPIO.output(self.__controlPinSix,GPIO.LOW)
        GPIO.output(self.__controlPinSeven,GPIO.LOW)
        GPIO.output(self.__controlPinEight,GPIO.LOW)
        GPIO.output(self.__controlPinNine,GPIO.LOW)
        GPIO.output(self.__controlPinTen,GPIO.LOW)
        GPIO.output(self.__controlPinEleven,GPIO.LOW)
        GPIO.output(self.__controlPinTwelve,GPIO.LOW)
        
        
    def turnAllRelaysOff(self):
        """
        Soll Relais mit angegebener Nummer ausschalten
        """
        
        # Setze beide Steuer-pins auf HIGH
        GPIO.output(self.__controlPinOne,GPIO.HIGH)
        GPIO.output(self.__controlPinTwo,GPIO.HIGH)
        GPIO.output(self.__controlPinThree,GPIO.HIGH)
        GPIO.output(self.__controlPinFour,GPIO.HIGH)
        GPIO.output(self.__controlPinFive,GPIO.HIGH)
        GPIO.output(self.__controlPinSix,GPIO.HIGH)
        GPIO.output(self.__controlPinSeven,GPIO.HIGH)
        GPIO.output(self.__controlPinEight,GPIO.HIGH)
        GPIO.output(self.__controlPinNine,GPIO.HIGH)
        GPIO.output(self.__controlPinTen,GPIO.HIGH)
        GPIO.output(self.__controlPinEleven,GPIO.HIGH)
        GPIO.output(self.__controlPinTwelve,GPIO.HIGH)
        
        
        
    def turnOnRelayOne(self):
        """
        Schaelt Relais 1 an
        """
        
        # Setze Steuer-pins auf LOW
        GPIO.output(self.__controlPinOne,GPIO.LOW)
        GPIO.output(self.__controlPinTwo,GPIO.LOW)
    def turnOffRelayOne(self):
        """
        Schaelt Relais 1 aus
        """
        
        # Setze Steuer-pins auf HIGH
        GPIO.output(self.__controlPinOne,GPIO.HIGH)
        GPIO.output(self.__controlPinTwo,GPIO.HIGH)
        
    def turnOnRelayTwo(self):
        """
        Schaelt Relasi 2 an
        """
        
        # Setze beide Steuer-pins auf LOW
        GPIO.output(self.__controlPinTwo,GPIO.LOW)
        
    def turnOffRelayTwo(self):
        """
        Schaelt Relasi 2 aus
        """
        
        # Setze Steuer-pins auf HIGH
        GPIO.output(self.__controlPinTwo,GPIO.HIGH)
        
    def turnOnRelayThree(self):
        """
        Schaelt Relais 3 an
        """
        
        # Setze Steuer-pins auf LOW
        GPIO.output(self.__controlPinThree,GPIO.LOW)
        
    def turnOffRelayThree(self):
        """
        Schaelt Relais 3 aus
        """
        
        # Setze teuer-pins auf HIGH
        GPIO.output(self.__controlPinThree,GPIO.HIGH)
        
    def turnOnRelayFour(self):
        """
        Schaelt Relais 4 an
        """
        
        # Setze Steuer-pins auf LOW
        GPIO.output(self.__controlPinFour,GPIO.LOW)
        
    def turnOffRelayFour(self):
        """
        Schaelt Relais 4 aus
        """
        
        # Setze Steuer-pins auf HIGH
        GPIO.output(self.__controlPinFour,GPIO.HIGH)
        
    def turnOnRelayFive(self):
        """
        Schaelt Relais 5 an
        """
        
        # Setze Steuer-pins auf LOW
        GPIO.output(self.__controlPinFive,GPIO.LOW)
        
    def turnOffRelayFive(self):
        """
        Schaelt Relais 5 aus
        """
        
        # Setze Steuer-pins auf HIGH
        GPIO.output(self.__controlPinFive,GPIO.HIGH)
        
    def turnOnRelaySix(self):
        """
        Schaelt Relais 6 an
        """
        
        # Setze Steuer-pins auf LOW
        GPIO.output(self.__controlPinSix,GPIO.LOW)
        
    def turnOffRelaySix(self):
        """
        Schaelt Relais 6 aus
        """
        
        # Setze Steuer-pins auf HIGH
        GPIO.output(self.__controlPinSix,GPIO.HIGH)
    
    def turnOnRelaySeven(self):
        """
        Schaelt Relais 7 an
        """
        
        # Setze Steuer-pins auf LOW
        GPIO.output(self.__controlPinSeven,GPIO.LOW)
        
    def turnOffRelaySeven(self):
        """
        Schaelt Relais 7 aus
        """
        
        # Setze Steuer-pins auf HIGH
        GPIO.output(self.__controlPinSeven,GPIO.HIGH)
        
    def turnOnRelayEight(self):
        """
        Schaelt Relais 8 an
        """
        
        # Setze Steuer-pins auf LOW
        GPIO.output(self.__controlPinEight,GPIO.LOW)
        
    def turnOffRelayEight(self):
        """
        Schaelt Relais 8 aus
        """
        
        # Setze Steuer-pins auf HIGH
        GPIO.output(self.__controlPinEight,GPIO.HIGH)
        
    def turnOnRelayNine(self):
        """
        Schaelt Relais 9 an
        """
        
        # Setze Steuer-pins auf LOW
        GPIO.output(self.__controlPinNine,GPIO.LOW)
        
    def turnOffRelayNine(self):
        """
        Schaelt Relais 9 aus
        """
        
        # Setze Steuer-pins auf HIGH
        GPIO.output(self.__controlPinNine,GPIO.HIGH)

    def turnOnRelayTen(self):
        """
        Schaelt Relais 10 an
        """
        
        # Setze Steuer-pins auf LOW
        GPIO.output(self.__controlPinTen,GPIO.LOW)
        
    def turnOffRelayTen(self):
        """
        Schaelt Relais 10 aus
        """
        
        # Setze Steuer-pins auf HIGH
        GPIO.output(self.__controlPinTen,GPIO.HIGH)

    def SwitchNOOne(self):
        """
        Schaelt Relais 11 extern
        """
        
        # Setze Steuer-pins auf LOW
        GPIO.output(self.__controlPinEleven,GPIO.LOW)
        
    def SwitchNCOne(self):
        """
        Schaelt Relais 11 intern
        """
        
        # Setze Steuer-pins auf HIGH
        GPIO.output(self.__controlPinEleven,GPIO.HIGH)
        
    def SwitchNOTwo(self):
        """
        Schaelt Relais 12 extern
        """
        
        # Setze Steuer-pins auf LOW
        GPIO.output(self.__controlPinTwelve,GPIO.LOW)
        
        
    def SwitchNCTwo(self):
        """
        Schaelt Relais 12 intern
        """
        
        # Setze Steuer-pins auf HIGH
        GPIO.output(self.__controlPinTwelve,GPIO.HIGH)

    def HyDryRelais(self):
        GPIO.output(self.__controlPinThree,GPIO.LOW)
        GPIO.output(self.__controlPinFive,GPIO.LOW)
        GPIO.output(self.__controlPinNine,GPIO.LOW)
        
    def EdgeRelais(self):
        GPIO.output(self.__controlPinFour,GPIO.LOW)
        GPIO.output(self.__controlPinThree,GPIO.LOW)
        GPIO.output(self.__controlPinTen,GPIO.LOW)
        GPIO.output(self.__controlPinNine,GPIO.LOW)
        GPIO.output(self.__controlPinFive,GPIO.LOW)

    def HyDryRelaisOff(self):
        GPIO.output(self.__controlPinThree,GPIO.HIGH)
        GPIO.output(self.__controlPinNine,GPIO.HIGH)
        GPIO.output(self.__controlPinFive,GPIO.HIGH)
        
    def EdgeRelaisOff(self):
        GPIO.output(self.__controlPinFour,GPIO.HIGH)
        GPIO.output(self.__controlPinThree,GPIO.HIGH)
        GPIO.output(self.__controlPinTen,GPIO.HIGH)
        GPIO.output(self.__controlPinNine,GPIO.HIGH)
        GPIO.output(self.__controlPinFive,GPIO.HIGH)
        
        
class Actuators():
    """
    Klasse, welche alle Hardware-Elemente enthaelt, die vom Pi aus steuerbar sind und nicht zur aufzeichnung von Messweerten dienen
    """
    
    def __init__(self,isIntendedToUseRelais):
        """
        Legt fest, wleche hardware angeschlossen ist.
        """     
        
        # Gibt an, ob Relais verwendet werden soll
        self.__isIntendedToUseRelais = isIntendedToUseRelais
        
        if self.__isIntendedToUseRelais == True:
            #Relais, welches ueber die Pins Nummer angesteuert wird. (17 relais 1, 22 relais 2 usw.)
            self.__myRelay = Relais(17,0,27,22,14,6,10,18,8,24,7,25)
        
        
    def getRelais(self):
        """
        Gibt Relais-Objekt zurueck
        """
        
        if self.__isIntendedToUseRelais == True:
            return self.__myRelay
        else:
            pass # TODO: hier Fehler ausloesen
    
    def getActuators(self):
        """
        Soll direkten Zugriff auf die Aktoren des Systems gewaehren, falls benoetigt.
        """
        
        return self.__myActuators
    
    
def main():
        
    if(__name__ == "__main__"):
        
        #Soll das Panel ueber USB (True) oder Serieler Schnittstelle(False) ausgelesen werden?
        isIntededToUseScaleUSB = True
        
        #werden die Relais mit verwendet?
        goingToUseRelais = True
        
        #Initialisierung derr Hauptanwendung
        mainApplication = MainGUI(goingToUseRelais,isIntededToUseScaleUSB)
        
        #Start der Hauptanwendung
        mainApplication.startMainloop()
        
        # belegte gpio-pins wieder freigeben
        GPIO.cleanup()
        print('Programm erfolgreich beendet, vielen Dank für Ihren Besuch.')

            

main()
